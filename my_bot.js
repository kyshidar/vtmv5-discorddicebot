/*

    Author:         Karl Störmer
    GitLab:         @kyshidar
    Version:        1.2.1
    Last Updated:   21.03.2020

*/

const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./auth.json');

client.on('ready', () => {
    client.user.setAvatar(auth.avatar);
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (receivedMessage) => {
    if (receivedMessage.author.bot) {
        return;
    }

    if (receivedMessage.content.startsWith("!")) {
        processCommand(receivedMessage);
    }
});

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(1);
    let splitCommand = fullCommand.split(" ");
    let primaryCommand = splitCommand[0];
    let arguments = splitCommand.slice(1);

    //RegExValidation
    const regExBasicRoll = /^([D])(0*([1-9]|10)?)$/i;
    const regExRollWithHunger = /^[D]0*?([1-9]|10)[H]0*[1-5]$/i;
    const regExRouseCheck = /^(R|rouse)(0*[1-3]?)*$/i;
    const regExHelpCommand = /^(H|help)$/i;

    console.log("Command received: " + primaryCommand);
    console.log("Arguments: " + arguments); // There may not be any arguments

    if (primaryCommand.match(regExBasicRoll)) {
        basicRollCommand(receivedMessage, primaryCommand);
    } else if (primaryCommand.match(regExRollWithHunger)) {
        extendedRollCommand(receivedMessage, primaryCommand);
    } else if(primaryCommand.match(regExRouseCheck)){
        rouseCheckCommand(receivedMessage, primaryCommand);
    } else if (primaryCommand.match(regExHelpCommand)) {
        helpCommand(receivedMessage);
    }
}

const numberPattern = /0*?\d{1,2}/g;
let successes, fails;

//
//----------------------------------------------------------------------------------------------------------------------
// Commands the bots can execute
//

function basicRollCommand(receivedMessage, primaryCommand) {

    let diceNumber = primaryCommand.match(numberPattern);
    if (diceNumber === null) {
        diceNumber = 1;
    }
    let diceResults = roll(diceNumber);
    let counter = countSuccessesAndFails(diceResults);
    successes = addSuccessesForCrit(counter[0], diceResults);
    fails = counter[1];
    let resultString = diceResults.join(", ");

    let reply = "\nRolled: *" + diceNumber + "* dice"
        + "\n  Results: *" + resultString
        + "*\n  Successes: *" + successes
        + "*\n  Fails: *" + fails + "*";
    console.log(reply);
    receivedMessage.reply(reply);

}

function extendedRollCommand(receivedMessage, primaryCommand) {

    let totalDiceNumber = parseInt(primaryCommand.match(numberPattern)[0],10);
    let hungerLevel = parseInt(primaryCommand.match(numberPattern)[1],10);
    let regularDiceNumber = totalDiceNumber - hungerLevel;
    if (totalDiceNumber < hungerLevel) {
        hungerLevel = totalDiceNumber;
        regularDiceNumber = 0;
    }

    let regularDiceResults = roll(regularDiceNumber);
    let hungerDiceResults = roll(hungerLevel);
    let totalDiceResults = regularDiceResults.concat(hungerDiceResults);

    let regularCounter = countSuccessesAndFails(regularDiceResults);
    let hungerCounter = countSuccessesAndFails(hungerDiceResults);

    successes = addSuccessesForCrit(regularCounter[0] + hungerCounter[0], totalDiceResults);
    fails = regularCounter[1] + hungerCounter[1];

    let hungerModifier = checkForMessyCriticalAndBestialFailure(hungerDiceResults);
    let criticalModifier = isCritical(totalDiceResults);

    let regularResultString = regularDiceResults.join(", ");
    let hungerResultString = hungerDiceResults.join(", ");

    let reply = "\nRolled: *" + totalDiceNumber + "* dice while Hunger **@" + hungerLevel
        + "**\n  Results: *" + (regularDiceResults.length === 0 ? "nothing" :regularResultString)
        + "* on regular dice and *" + hungerResultString + "* on Hunger Dice"
        + "\n  Successes Total: *" + successes
        + "*\n  Fails Total: *" + fails + "*"
        + (hungerModifier[0] && criticalModifier ?
            ("\n  A  **check win** with " + countCriticalSuccesses(hungerDiceResults)
                + " Critical Success(es) on Hunger Dice will lead to a **Messy Critical**.") : "")
        + (hungerModifier[1] ?
            ("\n  A **check fail** with " + countCriticalFails(hungerDiceResults)
                + " Critical Fail(s) on Hunger Dice will cause a **Bestial Failure**.") : "");
    console.log(reply);
     receivedMessage.reply(reply);

}

function rouseCheckCommand(receivedMessage, primaryCommand) {

    let diceNumber = primaryCommand.match(numberPattern);
    if (diceNumber === null) {
        diceNumber = 1;
    }
    let diceResults = roll(diceNumber);
    let rouseResults = countSuccessesAndFails(diceResults);

    let resultString = diceResults.join(", ");

    let reply = "\nRolled: *" + diceNumber + "* dice"
        + "\n  Result(s): *" + resultString
        + "*\n  Successes: *" + rouseResults[0]
        + "*\n  Your Hunger increases by **" + rouseResults[1] + "**";
    console.log(reply);
    receivedMessage.reply(reply);

}

function helpCommand(receivedMessage) {
    let reply = "\nAvailable Commands: "
        + "\n  **!Dx**: Roll x (1-10) dice - given no number 1 die gets rolled."
        + "\n  **!DxHy**: Roll x (1-10) dice - y (1-5) dice are substituted for Hunger Dice."
        + "\n  **!Rx** or **!ROUSEx**: Roll x (1-3) Hunger Dice - x can be omitted."
        +"\n  *Upper and lower capitalization does not matter.*";
    receivedMessage.reply(reply);
}

//
//----------------------------------------------------------------------------------------------------------------------
// Functions for the Commands
//
function roll(diceNumber){
    let result = [];
    for (let i=0; i<diceNumber; i++) {
        result[i] = Math.floor((Math.random() * 10) + 1);
    }
    return result;
}

function countSuccessesAndFails(diceResults) {
    successes = 0;
    fails = 0;
    for (let i = 0; i< diceResults.length; i++){
        if (diceResults[i]>5) {
            successes++;
        } else {
            fails++;
        }
    }
    return [successes, fails];
}

function checkForMessyCriticalAndBestialFailure(hungerDiceResults){
    let a = 0, b = 0;
    let checkSuccess = false, checkFail = false;
    for (let i = 0; i < hungerDiceResults.length; i++) {
        if (hungerDiceResults[i] === 10){
            a++;
        }
        if (hungerDiceResults[i] === 1){
            b++;
        }
    }
    if (a > 0) {checkSuccess = true}
    if (b > 0) {checkFail = true}
    return[checkSuccess, checkFail]
}

function isCritical (diceResults){
    let critCounter = 0;
    for (let i = 0; i < diceResults.length; i++) {
        if (diceResults[i] === 10) {
            critCounter++;
        }
    }
    return critCounter > 1;
}

function countCriticalSuccesses (diceResults){
    let critCounter = 0;
    for (let i = 0; i < diceResults.length; i++) {
        if (diceResults[i] === 10) {
            critCounter++;
        }
    }
    return critCounter;
}

function countCriticalFails (diceResults){
    let critCounter = 0;
    for (let i = 0; i < diceResults.length; i++) {
        if (diceResults[i] === 1) {
            critCounter++;
        }
    }
    return critCounter;
}

function addSuccessesForCrit(successes, diceResults){
    let addition = 0;
    let crits = countCriticalSuccesses(diceResults);
    if (crits > 1){
        addition = crits - (crits % 2)
    }
    return successes + addition;
}

//
//----------------------------------------------------------------------------------------------------------------------
//
client.login(auth.token);
